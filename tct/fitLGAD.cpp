{
    // Macro to extract VGL, VFD and other parameters from TCT scan
    // run after the macro for loading data (probably simple2d.cpp)

#define kDIRECTIONX 0
#define kDIRECTIONY 1
#define kFLUENCE_0E14 0
#define kFLUENCE_8E14 1
#define kFLUENCE_15E14 2
#define kFLUENCE_25E14 3
#define kFLUENCE_25E14 3

    int scanDirection = kDIRECTIONY;    // define direction of the scan here
    int iFl = kFLUENCE_0E14 ;           // select between different fit ranges by setting this variable

    float xs[4];  // positions of rising/falling edges in charge profile at Vmax {risingEdge1, fallingEdge1, risingEdge2, fallingEdge2}
    // define border widths for LGAD/gap region, which will be ignored in fitting pol0
    // LGAD 1 will be fitted inside {xs[0] + borderLGADum, xs[1] - borderLGADum} etc.
    int borderLGADum = 22;
    int borderGAPum = 20;

    float p = 0.5;      // fraction of max height to determine rising/falling edge of profile

// ranges for linear fits on charge(voltage) graphs for both LGADs and GAP
    vector<double> xGLmin = {0,0,0,0};                    // range for LGAD during gain layer depletion
    vector<double> xGLmax = {18,18,18,18};
    double xGLextMin=xGLmin[iFl], xGLextMax=30;    // same, but extended for plotting


    // xDeplMin is computed on the fly from rising edge of charge vs. voltage
    vector<double> xDeplMin = {0,22,22,20};               // range for LGAD during bulk depletion
    vector<double> xDeplMax = {30,33,42,47};
    double xDeplExtMin=10, xDeplExtMax=70;              // same, but extended for plotting


    vector<double> xFDmin = {30,34,38,20};               // range for LGAD during full depletion
    vector<double> xFDmax = {40,40,44,47};
    double xFDextMin=30, xFDextMax=70;              // same, but extended for plotting


    vector<double> xDeplGapMin = {0,20,20,20};               // range for PIN during bulk depletion
    vector<double> xDeplGapMax = {20,35,42,35};



    vector<double> xODmin = {50,90, 90,100};            // range for LGAD during overdepletion
    vector<double> xODmax = {100,140, 140,300};
    double xODextMin=40, xODextMax=200;                 // same, but extended for plotting

    // getting xs values from charge profile at Vmax, select profile here
    int iTemplateWF=0, iTemplateX=0;
    int iTemplateVol = 0;   // 0 .. assuming first voltage is the highest
    {
        TH1F* h;
        if (scanDirection == kDIRECTIONX) h = x1d[iTemplateWF][iTemplateVol][iTemplateX];
        else h = y1d[iTemplateWF][iTemplateVol][iTemplateX];

        xs[0] = Profile_RisingEdgeRel(h, p, 1);     // (TH1, fraction, first)
        xs[1] = Profile_FallingEdgeRel(h, p, 0, 1); // (TH1, fraction, firstBin, first)
        xs[2] = Profile_RisingEdgeRel(h, p, 0);
        xs[3] = Profile_FallingEdgeRel(h, p, 0, 0);
    }
    for (int i=0; i<4; i++) cout << xs[i] << endl;

    //ranges to define pol0 fits
    double x0 = xs[0] + borderLGADum;   // LGAD1
    double x1 = xs[1] - borderLGADum;

    double x2 = xs[1] + borderGAPum;    // gap
    double x3 = xs[2] - borderGAPum;

    double x4 = xs[2] + borderLGADum;   // LGAD 2
    double x5 = xs[3] - borderLGADum;

///////////////  Fitting LGAD and GAP plateaus with pol0
    vector<float> A1, A2;           // gain: LGAD/gap
//     vector<float> A2;               // gain 2: LGAD2/gap
    vector<float> q1, q2;               // charge in LGADs 1 and 2
//     vector<float> q2;               // charge in LGAD2
    vector<float> qGap;             // charge in gap

    TF1* LGAD1fit = new TF1("LGAD1fit", "pol0", x0, x1);
    TF1* LGAD2fit = new TF1("LGAD2fit", "pol0", x4, x5);
    TF1* GAPfit = new TF1("GAPfit", "pol0", x2, x3);

    gROOT->SetBatch(true);  // do not draw histo after fitting
    for (int i=0; i < U.size(); i++) { // fits the plateaus for each measurement

//         TH1F* h = x1d[0][i][0];
        TH1F* h = 0;
        if (scanDirection == kDIRECTIONX) h = x1d[0][i][0];
        else h = y1d[0][i][0];

        h->Fit(LGAD1fit, "RQ");
        h->Fit(GAPfit, "RQ+");
        h->Fit(LGAD2fit, "RQ+");

        float charge1 = LGAD1fit->GetParameter(0);
        float charge2 = LGAD2fit->GetParameter(0);
        float chargeGap = GAPfit->GetParameter(0);

//         cout << U[i] << "\t" << charge1 << "\t" << chargeGap << "\t" << charge2 << endl;

        A1.push_back(charge1/chargeGap);
        A2.push_back(charge2/chargeGap);
        q1.push_back(charge1);
        q2.push_back(charge2);
        qGap.push_back(chargeGap);
    }

    gROOT->SetBatch(false);

    double q1Max = TMath::MaxElement(q1.size(), &q1[0]);
    double q2Max = TMath::MaxElement(q2.size(), &q2[0]);
    double qGapMax = TMath::MaxElement(qGap.size(), &qGap[0]);
    double yMax = TMath::Max(q1Max, q2Max);
    yMax = TMath::Max(yMax, qGapMax);

    auto gq1 = new TGraph(U.size(), &U[0], &q1[0]);
    gq1->SetTitle("LGAD1 signal;U(V);Charge");
    SortGraph(gq1, true);   // sort points in ascending order in x (voltage)
    gq1->GetYaxis()->SetRangeUser(0, yMax);

    auto gqGap = new TGraph(U.size(), &U[0], &qGap[0]);
    gqGap->SetTitle("GAP signal;U(V);Charge");
    SortGraph(gqGap, true);   // sort points in ascending order in x (voltage)
    gqGap->GetYaxis()->SetRangeUser(0, yMax);

    auto gq2 = new TGraph(U.size(), &U[0], &q2[0]);
    gq2->SetTitle("LGAD2 signal;U(V);Charge");
    SortGraph(gq2, true);   // sort points in ascending order in x (voltage)
    gq2->GetYaxis()->SetRangeUser(0, yMax);

    auto gA1 = new TGraph(U.size(), &U[0], &A1[0]);
    gA1->SetTitle("Gain LGAD1 (LGAD1/gap);U(V);A");
    SortGraph(gA1, true);   // sort points in ascending order in x (voltage)

    auto gA2 = new TGraph(U.size(), &U[0], &A2[0]);
    gA2->SetTitle("Gain LGAD2 (LGAD2/gap);U(V);A");
    gA2->SetLineColor(kBlue);
    SortGraph(gA2, true);   // sort points in ascending order in x (voltage)

    double pDeplMin = 0.02, pDeplMax = 0.85;
    xDeplMin[iFl] = GraphGetEdgeRel(gq1, pDeplMin, true, false);
//     xDeplMax[iFl] = GraphGetEdgeRel(gq1, pDeplMax, true, false);
    cout << xDeplMin[iFl] << " " << xDeplMax[iFl] << endl;

///////////////////////////// Linear fits for charge(voltage) profiles and intersections
    // y1 = k1*x + n1, y2 = k2*x + n2
    // Intersection = -(n2-n1)/(k2-k1)
    // VGL .. intersection of fGL and fDepl
    // VFD .. intersection of fDepl and fOD

    TF1 *fGL        = new TF1("fGL",        "pol1", xGLmin[iFl],     xGLmax[iFl]);    // gain layer depletion in LGAD
    TF1 *fDepl      = new TF1("fDepl",      "pol1", xDeplMin[iFl],   xDeplMax[iFl]);  // bulk depletion in LGAD
    TF1 *fDeplGap   = new TF1("fDeplGap",   "pol1", xDeplGapMin[iFl],   xDeplGapMax[iFl]);  // bulk depletion in gap
    TF1 *fFD        = new TF1("fFD",        "pol1", xFDmin[iFl],     xFDmax[iFl]);    // full depletion in LGAD
    TF1 *fOD        = new TF1("fOD",        "pol1", xODmin[iFl],     xODmax[iFl]);    // overdepletion in LGAD
    TF1 *f1, *f2, *f3, *f4;

    gq1->Fit(fGL, "RQ+");
    gq1->Fit(fDepl, "RQ+");
    gq1->Fit(fFD, "RQ+");
    gq1->Fit(fOD, "RQ+");

    gq2->Fit(fGL, "RQ+");
    gq2->Fit(fDepl, "RQ+");
    gq2->Fit(fFD, "RQ+");
    gq2->Fit(fOD, "RQ+");

    gqGap->Fit(fDeplGap, "RQ+");
    gqGap->Fit(fOD, "RQ+");

    // LGAD1
    f1=gq1->GetFunction("fGL"); f2=gq1->GetFunction("fDepl"); f3=gq1->GetFunction("fOD"); f4=gq1->GetFunction("fFD");
    f1->SetRange(xGLextMin, xGLextMax);     f1->SetLineColor(kGreen);
    f2->SetRange(xDeplExtMin, xDeplExtMax); f2->SetLineColor(kRed);
    f3->SetRange(xODextMin, xODextMax);     f3->SetLineColor(kBlue);
    f4->SetRange(xFDextMin, xFDextMax);     f4->SetLineColor(kMagenta);
    double vgl1 = -(f1->GetParameter(0) - f2->GetParameter(0)) / (f1->GetParameter(1) - f2->GetParameter(1));
//     double vfd1 = -(f2->GetParameter(0) - f3->GetParameter(0)) / (f2->GetParameter(1) - f3->GetParameter(1));
    double vfd1 = -(f4->GetParameter(0) - f3->GetParameter(0)) / (f4->GetParameter(1) - f3->GetParameter(1));

    // LGAD2
    f1=gq2->GetFunction("fGL"); f2=gq2->GetFunction("fDepl"); f3=gq2->GetFunction("fOD"); f4=gq2->GetFunction("fFD");
    f1->SetRange(xGLextMin, xGLextMax);     f1->SetLineColor(kGreen);
    f2->SetRange(xDeplExtMin, xDeplExtMax); f2->SetLineColor(kRed);
    f3->SetRange(xODextMin, xODextMax);     f3->SetLineColor(kBlue);
    f4->SetRange(xFDextMin, xFDextMax);     f4->SetLineColor(kMagenta);
    double vgl2 = -(f1->GetParameter(0) - f2->GetParameter(0)) / (f1->GetParameter(1) - f2->GetParameter(1));
//     double vfd2 = -(f2->GetParameter(0) - f3->GetParameter(0)) / (f2->GetParameter(1) - f3->GetParameter(1));
    double vfd2 = -(f4->GetParameter(0) - f3->GetParameter(0)) / (f4->GetParameter(1) - f3->GetParameter(1));

    // gap
    f2=gqGap->GetFunction("fDeplGap"); f3=gqGap->GetFunction("fOD");
    f2->SetRange(xDeplExtMin, xDeplExtMax); f2->SetLineColor(kRed);
    f3->SetRange(xODextMin, xODextMax);     f3->SetLineColor(kBlue);
    double vfdGap = -(f2->GetParameter(0) - f3->GetParameter(0)) / (f2->GetParameter(1) - f3->GetParameter(1));

    //////////// Find voltage when signals from LGAD1 or LGAD2 and gap are equal
    double Ugain1_1 = GraphRisingEdgeAbs (gA1, 1.0, false);   // graph must be sorted in ascending order to work correctly
    double Ugain1_2 = GraphRisingEdgeAbs (gA2, 1.0, false);   // graph must be sorted in ascending order to work correctly

    //////////// Plotting

    TCanvas* c2 = new TCanvas();
    c2->Divide(2, 2);

    c2->cd(1);
    gq1->Draw("AC*");
    gPad->SetGrid(1,1);

    c2->cd(2);
    gqGap->Draw("AC*");
    gPad->SetGrid(1,1);

    c2->cd(3);
    gq2->Draw("AC*");
    gPad->SetGrid(1,1);

    c2->cd(4);
    gA1->Draw("AC*");
    gA2->Draw("C*");
    gPad->SetGrid(1,1);

    TLatex tex;
    double texX = vfd1, texY1 = 0.25, texY2 = 0.7;
    c2->cd(1);
    tex. DrawLatex(texX, texY1*yMax, Form("V_{GL} = %.1lf V", vgl1));
    tex. DrawLatex(texX, texY2*yMax,  Form("V_{FD} = %.1lf V", vfd1));
    c2->cd(2);
    tex. DrawLatex(texX, texY2*yMax,  Form("V_{FD} = %.1lf V", vfdGap));
    c2->cd(3);
    tex. DrawLatex(texX, texY1*yMax, Form("V_{GL} = %.1lf V", vgl2));
    tex. DrawLatex(texX, texY2*yMax,  Form("V_{FD} = %.1lf V", vfd2));
    c2->cd(4);
    tex. DrawLatex(texX, 1, Form("V_{LGAD=GAP} = %.1lf V / %.1lf V", Ugain1_1, Ugain1_2));


    // Draw sampling regions
    TLine line;
    TMarker marker; marker.SetMarkerStyle(20);
    vector<double> xLine = {x0,x1,x2,x3,x4,x5}; // fit ranges
    c.push_back(new TCanvas());
    int npads = 6;
    c.back()->Divide(npads/2, 2);
    for (int i=0; i<npads; i++){
        int index = Nvol*i/npads;
        TH1 * h = ((scanDirection == kDIRECTIONX) ? x1d[0][index][0] : y1d[0][index][0]);
        c.back()->cd(i+1);
        h->Draw();
        for (auto ix : xLine) line.DrawLine(ix, 0, ix, h->GetMaximum());
        marker.DrawMarker(xs[0], p*q1[index]);
        marker.DrawMarker(xs[1], p*q1[index]);
        marker.DrawMarker(xs[2], p*q1[index]);
        marker.DrawMarker(xs[3], p*q1[index]);
    }

    c2->RaiseWindow();
    c2->SetWindowSize(1600,1200);

/////////////////////////////////// Save output in text form in "output" folder

    // output dir
    TString outputDirName = "output";
    if (gSystem->AccessPathName(outputDirName.Data()) != 0){
        cout << "Creating output directory: " << outputDirName.Data() << endl;
        gSystem->MakeDirectory(outputDirName.Data());
    }
    // output file
    TString outputFileName = fileName;
    outputFileName.ReplaceAll(".rtct", ""); // Removes trailing ".rtct"

    // output time stamp
    std::time_t currentTime = std::time(nullptr);
    std::tm* timeInfo = std::localtime(&currentTime);
    char buffer[80];
    std::strftime(buffer, sizeof(buffer), "%Y-%m-%d", timeInfo);
    TString timestamp = buffer;

    TString outputPath = outputDirName + "/" + outputFileName + "_" + timestamp + ".txt";
    std::ofstream outputFile(outputPath.Data());
    if (!outputFile.is_open()) std::cerr << "Error opening file!" << std::endl;

    outputFile << "VGL1:" << vgl1 << endl;
    outputFile << "VFD1:" << vfd1 << endl;
    outputFile << "VGL2:" << vgl2 << endl;
    outputFile << "VFD2:" << vfd2 << endl;
    outputFile << "VFDgap:" << vfdGap << endl;
    outputFile << "Gain 1.0 #1: (" << Ugain1_1 << " V)" << endl;
    outputFile << "Gain 1.0 #2: (" << Ugain1_2 << " V)" << endl;

    outputFile.close();

    std::cout << "Data has been written to the file: " << outputPath << std::endl;


}

