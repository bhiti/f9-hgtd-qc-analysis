#!/bin/bash

echo "Starting setup, count to ten ... "

# Compile our custom library with helper functions (PSTCT-macro_root6_C.so). It does not need to be run more than once during the directory's lifetime
macroname=PSTCT-macro_root6.C

# Run CERN ROOT and execute the ROOT script
# initial cleanup
macroname_without_extension=${macroname%.C}
macroD="${macroname_without_extension}_C.d"
macroSO="${macroname_without_extension}_C.so"
macroPCM="${macroname_without_extension//-/_}_C_ACLiC*"
rm $macroD
rm $macroSO
rm $macroPCM

# We are supressing all error messages. What could go wrong?
root -l -b -q $macroname++ > /dev/null 2>&1

# Cleanup unneeded files
rm $macroD
#rm $macroPCM

# create input and output dirs
directory="data"
if [ ! -d "$directory" ]; then
    mkdir "$directory"
fi
directory="output"
if [ ! -d "$directory" ]; then
    mkdir "$directory"
fi

echo "Setup completed"
