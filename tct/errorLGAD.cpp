{
    // Fit S-curves on charge profiles around the gap and extract the width of the gap

    //ranges for S-curve fits
    int sCurveRangeLGAD = 110;   // fit range from falling edge in lgad region
    int sCurveRangeGap = 30;     // fit range from falling edge in gap region

    double sigma = 20;      // estimated sigma of the error functions
    double thrHeight = 40;  // threshold for minimal required lgad signal size

    double z0 = xs[1] - sCurveRangeLGAD;
    double z1 = xs[1] + sCurveRangeGap;

    double z2 = xs[2] - sCurveRangeGap;
    double z3 = xs[2] + sCurveRangeLGAD;
//

    // macro can be executed after simple2d.cpp and fitLGAD.cpp
    vector<float> profileHeight;
    vector<float> gapWidth;
    vector<float> gapCenter;

    gROOT->SetBatch(true);

    for (int i=0; i<U.size(); i++) {

        // q1, q2, qGap are vector<float> filled in fitLGAD.cpp
//         TH1F* h = x1d[0][i][0];
        TH1F* h = 0;
        if (scanDirection == kDIRECTIONX) h = x1d[0][i][0];
        else h = y1d[0][i][0];

        TF1 *ff = new TF1("ff","1*TMath::Erf((x-[0])/-[1])*[2]-[3]", z0, z1);
        TF1* ff2 = new TF1("ff2", "1*TMath::Erf((x-[0])/[1])*[2]-[3]", z2, z3);

        double p0_1 = xs[1];  // S-curve center in x
        double p1_1 = sigma;   // S-curve sigma
        double p2_1 = q1[i];  // scaling factor
        double p3_1 = -p2_1;  // offset

        double p0_2 = xs[2];  // S-curve center in x
        double p1_2 = p1_1;   // S-curve sigma
        double p2_2 = p2_1;  // scaling factor
        double p3_2 = p3_1;  // offset

        double midY = (q1[i] + qGap[i]) / 2;
        double width = -1;
        if (q1[i] < thrHeight) gapWidth.push_back(0);           // if LGAD signal too small
        else if (qGap[i] < q1[i]){
            ff->SetParameters(p0_1, p1_1, p2_1, p3_1);
            h->Fit(ff, "RQ");

            ff2->SetParameters(p0_2, p1_2, p2_2, p3_2);
            h->Fit(ff2, "RQ+");

            width = abs(abs(ff2->GetX(midY)) - abs(ff->GetX(midY)));
        }
        else{
            ff->SetParameters(p0_1, -p1_1, p2_1, p3_1); // minus at p1
            h->Fit(ff, "RWWQ");
            ff2->SetParameters(p0_2, -p1_2, p2_2, p3_2);
            h->Fit(ff2, "RWWQ+");

            width = abs(abs(ff2->GetX(midY)) - abs(ff->GetX(midY)));
        }

        gapWidth.push_back(width);

//         cout << U[i] << " V:\t" << width << " um" << endl;
//     h->Draw();
//     ff->Draw("same");
//     ff2->Draw("same");
    }

    gROOT->SetBatch(false);



///////////////////// Plot results

    auto g = new TGraph(U.size(), &U[0], &gapWidth[0]);
    g->SetTitle("width(U);U(V);width(#mum)");

    TH1F* widthHistogram = new TH1F("widthHistogram", "Width Histogram;Width (#mum); counts", 50, 0, 200);
    for (int i = 0; i < gapWidth.size(); i++) {
        if (gapWidth[i] <= 0) continue;
        else widthHistogram->Fill(gapWidth[i]);
    }

    // Create a canvas for the histogram
    TCanvas* c3 = new TCanvas("c3", "Width Histogram", 800, 600);
    c3->Divide(2,1);
    c3->cd(1); g->Draw("AC*");
    c3->cd(2); widthHistogram->Draw();


///////////////////// output all the widhts in text:
//     std::ofstream outputFile("width.txt");
//
//     if (!outputFile.is_open()) {
//         std::cerr << "Error opening file!" << std::endl;
//         return 1;
//     }
//
//     // Save the original buffer
//     std::streambuf* origCoutBuffer = std::cout.rdbuf();
//
//     // Redirect cout to the file
//     std::cout.rdbuf(outputFile.rdbuf());
//
//     for (int i = 0; i < arraySize; ++i) {
//         std::cout << widths[i] << "\n";
//     }
//
//     std::cout.rdbuf(origCoutBuffer);
//
//     // Close the file
//     outputFile.close();
//
//     std::cout << "Data has been written to the file." << std::endl;

//////////////////// cout ERROR if abs(width[i] - width[0]) > 7
    for (int i = 0; i < U.size(); i++) {
        if (gapWidth[i] > 0 && (gapWidth[i]/gapWidth[0] > 2 || gapWidth[i]/gapWidth[0] < 0.5)) {
            std::cout << "\033[1;31m";
            std::cout << "BIG ERROR, a width does not match";
            std::cout << "\033[0m";
            std::cout << " at voltage: " << U[i] << "V" << "\n";
//             break;
        }
    }

}
