{
  // Macro to plot 2d (x,y) particulars TCT measurements from a single rtct file
  // Coders: Matic Terglav, Bojan Hiti
  // Use: root -l simple2d.cpp
  // Make sure PSTCT-macro_root6 is compiled (see below)

//   R__LOAD_LIBRARY(/home/f9cmos/tools/TCTAnalyse.V2.2/TCTAnalyse.sl);
  R__LOAD_LIBRARY(/home/f9cmos/tools/TCTAnalyse.V2.2/TCTAnalyse.sl);  // adjust your path here
  R__LOAD_LIBRARY(PSTCT-macro_root6_C.so);	// compile in separate root session with ".L PSTCT-macro_root6.C++"

  gStyle->SetPalette(kRainBow);
  gErrorIgnoreLevel=kError;   // Do not dump warnings

  TH1::AddDirectory(kFALSE);  // do not associate objects to any file

  int Nwf = 2;          // number of waveforms in file {nwf1, nwf2, nwf3, ... etc}
  int FileType = 2;     // file format --> 2
  vector<Double_t> sgn = {1, 1};     // pulse sign pos/neg
  Double_t tOffset = 24;     // pulse offset in ns from t=0; important for velocity calculation

  // Make sure to set Nwf >= 2 if using beam monitor
  bool beamMonitorActive = true;   // Set to true for applying beam monitor correction
//   bool beamMonitorActive = false;   // Set to true for applying beam monitor correction
  int chBeamMonitor=1;              // channel of the beam monitor. Usually 0 .. signal, 1 .. BM, 2 .. trigger etc.

  Float_t tIntStart = 0;      // starting time of pulse integration
  Float_t tIntLength = 6;    // duration of pulse integration (in ns)

//   TString fileName = "V2R12-37_1x2_0e14_293K_0-40V_Y.rtct";
  TString fileName = "V1R2 -21_1x2_1.5e15_293K_0-200V_Y7.rtct";
  TString filePath = "/home/f9cmos/HGTD-IT/data/preproduction/IHEP-IME/" + fileName;

  //
  // Load data and initialize containers
  //
  gROOT->SetBatch(true);

  if (gSystem->AccessPathName(filePath) != 0){
    cout << "Data file not found, aborting." << endl << filePath.Data() << endl;
    gSystem->Abort(1);
  }

  PSTCT *aa = new PSTCT((char*)(filePath.Data()) ,tOffset, FileType);
  aa->CorrectBaseLine(-5);  // shift baseline to center at 0 mV (calculated up to time 0 ns)
  aa->PrintInfo();
  Int_t Nvol  = aa->NU1;
  Int_t Nx    = aa->Nx;
  Int_t Ny    = aa->Ny;
  Float_t upX = TMath::Abs(Nx*aa->dx); upX = TMath::Max(upX, (Float_t)(1.));
  Float_t upY = TMath::Abs(Ny*aa->dy); upY = TMath::Max(upY, (Float_t)(1.));
  vector<Float_t> U(aa->U1.GetArray(), aa->U1.GetArray() + aa->U1.GetSize());
  vector<Float_t> I(aa->I1.GetArray(), aa->I1.GetArray() + aa->I1.GetSize());

  vector<int> activeCh;     // stores indices of active channels in data file (PSTCT::WFOnOff)
  for (int i=0; i<4; i++)
    if (aa->WFOnOff[i]) activeCh.push_back(i);

//   Book containers

  vector<vector<vector<MeasureWF*> > > w (Nwf, vector<vector<MeasureWF*> >(Nvol));  // waveforms
  vector<vector<TH2F*> >               c2d (Nwf);              // 2-d charge map

  for (int iwf=0; iwf<Nwf; iwf++){
    for (Int_t ivo = 0;ivo < Nvol; ivo++) {
      c2d[iwf].push_back( new TH2F(
        Form("c2d_%d_%d", iwf, ivo),
        Form("; x (#mum); y (#mum); charge (a. u.)"),
        Nx, 0, upX,
        Ny, 0, upY
      ));
      c2d[iwf].back()->SetStats(0);
    }
  }

  for (int iwf=0; iwf<Nwf; iwf++){
    float tlow = tIntStart;
    float thigh = tlow + tIntLength; // signal integration time
    for (Int_t ivo = 0;ivo < Nvol; ivo++) {
      for (Int_t ix = 0;ix < Nx; ix++) {
        w[iwf][ivo].push_back(aa->Projection(activeCh[iwf],1,ix,0,0,ivo,0,Ny)); //
        for (Int_t iy = 0;iy < Ny; iy++) {
          auto wf = w[iwf][ivo][ix]->GetHA(iy);  // this is a TH1F* with the waveform
          // if pulses in wf are negative you have to take it into account yourself
          int binLow  = wf->FindBin(tlow);
          int binHigh = wf->FindBin(thigh);
          Float_t q = wf->Integral(binLow, binHigh, "width");   // "width" multiplies the integral with the width of the histogram bin, see TH1
          q *= sgn[iwf];       // modify pulse sign/scale factor - if pulses are negative flip the sign of collected charge here (neg -> pos)

          if (iwf == chBeamMonitor && beamMonitorActive) q = wf->GetMaximum();

          c2d[iwf][ivo]->SetBinContent(ix+1, iy+1, q);
        }
      }
    }
  }

  cout << endl;

  gROOT->SetBatch(false);
  cout << "Data read in" << endl;

  cout << "Applying beam monitor correction" << endl;

  if (beamMonitorActive){
    for (Int_t ivo = 0;ivo < Nvol; ivo++) {
      c2d[0][ivo]->Divide(c2d[1][ivo]);
    }
  }
  cout << "Filling 1d histograms" << endl;

  vector<vector<vector<TH1F*> > > x1d (Nwf, vector<vector<TH1F*> >(Nvol));
  vector<vector<vector<TH1F*> > > y1d (Nwf, vector<vector<TH1F*> >(Nvol));

  for (int iwf=0; iwf<Nwf; iwf++){
    for (Int_t ivo = 0;ivo < Nvol; ivo++) {
      for (Int_t iy = 0;iy < Ny; iy++) {
        x1d[iwf][ivo].push_back((TH1F*)(GetTH1(c2d[iwf][ivo], "x",iy+1,iy+1)));
        TString htitle = Form ("Charge [y=%.0lf #mum] %.0lf V; x (#mum); charge (a. u.)", iy*aa->dy, U[ivo]);
        x1d[iwf][ivo].back()->SetTitle(htitle.Data());
      }

      for (Int_t ix = 0;ix < Nx; ix++) {
        y1d[iwf][ivo].push_back((TH1F*)(GetTH1(c2d[iwf][ivo], "y",ix+1,-1)));
        TString htitle = Form ("Charge [x=%.0lf #mum] %.0lf V; y (#mum); charge (a. u.)", ix*aa->dx, U[ivo]);
        y1d[iwf][ivo].back()->SetTitle(htitle.Data());
      }
    }
  }

  cout << "1d historgrams filled" << endl;

  gIV = new TGraph(U.size(), &U[0], &I[0]);
  gIV->SetTitle("I-V; U(V); I(A)");
//   gIV->GetYaxis()->SetRangeUser(0,300e-6);

  cout << "Plotting 2d" << endl;

  vector<TCanvas*> c;
  vector<int> plotPix = {0};    // channels to plot
  vector<int> plotV = {0};        // voltages to plot for each channel

  for (auto i : plotPix){
    for (auto iv : plotV){
      if (i>=Nwf) continue;
      if (iv >= Nvol) continue;

      c.push_back(new TCanvas());
      c.back()->Divide(3,2);
      c.back()->cd(1); c2d[i][iv]->Draw("COLZ");
      c.back()->cd(2); x1d[i][iv][Ny/2]->Draw();
      c.back()->cd(3); y1d[i][iv][0]->Draw();
      c.back()->cd(4); w[i][iv][Nx/2]->Draw(Ny/2,"");
      c.back()->cd(5); gIV->Draw("APL");

//       TString plotName;
//       plotName = Form("plots/ch%d.png", i);
//       c.back()->Print(plotName.Data());

    }
  }

  gPad = nullptr;

  gROOT->ProcessLine(".x fitLGAD.cpp");
//   gROOT->ProcessLine(".x errorLGAD.cpp");
}
