{
  // Analysis macro for knife edge scans
  // Extracts beam width (FWHM = 2.35 sigma) as a function of z-position
  // Run with "root -l focus.C"

  R__LOAD_LIBRARY(/home/f9cmos/tools/TCTAnalyse.V2.2/TCTAnalyse.sl);  // adjust your path here

  double tDelay = 25;
  int fileType = 2;

  double tInt = 7;  // pulse integration time
  double sCurveXmin = 0;    // lower border for S-curve fit range
  double sCurveXmax = 150;  // upper border for S-curve fit range


  TString fileName = "/home/f9cmos/HGTD-IT/data/focus/focus1.rtct";

  PSTCT aa((char*)(fileName.Data()), tDelay, fileType); // podatki

  aa.CorrectBaseLine(0);   // Baseline correction
  aa.PrintInfo();

  const int Nz = aa.Nz;   // number of steps in z
  vector<MeasureWF*> w;

  bool scanDirection = 0;   // 0 .. x, 1 ..y

  if (scanDirection == false){
  // x-scan
    for (Int_t iz = 0; iz<Nz; iz++) {
      w.push_back(aa.Projection(0,0,0,0,iz,0,0,aa.Nx)); // Projekcije v MeasureWF na x za posamezne z-je
    }
  }
  else {
    // y-scan
    for (Int_t iz = 0; iz<Nz; iz++) {
      w.push_back(aa.Projection(0,1,0,0,iz,0,0,aa.Ny)); // Projekcije v MeasureWF na y za posamezne z-je
    }
  }
  vector<TGraph *> gc;

  for (Int_t iz = 0; iz < Nz; iz++) {
    gc.push_back (w[iz]->CCE(0, tInt)); // charge vs y for given z
    gc.back()->SetTitle(Form("z = %.0lf um; x (#mum)", iz*aa.dz));
  }

  cout << "Profiles generated" << endl;

  // plot charge profiles
//   TCanvas *c1 = new TCanvas();
//   gc[10]->Draw("APL");
//   for (Int_t iz = 0; iz < Nz; iz++) {
//     gc[iz]->Draw("same");
//   }

  // plot waveform
  TCanvas *c2 = new TCanvas();
  int drawY=10, drawZ=0;
  w[drawZ]->Draw(drawY, "");

// Error function for fitting charge profiles

  TF1 *ff = new TF1("ff","1*TMath::Erf((x-[0])/[1])*[2]-[3]", sCurveXmin, sCurveXmax);
  double p0 = 50;  // S-curve center in x
  double p1 = 20;   // S-curve sigma
  double p2 = 150;  // scaling factor = profile height/2
  double p3 = -p2;  // offset

  vector<double> width, xx;

  cout << "Fitting erf" << endl;
  for(int j=0; j<Nz; j++)
  {
    // Fit each charge profile with an S-curve and get its width

    ff->SetParameters(p0, p1, p2, p3);  // set initial parameters
    gc[j]->Fit("ff","R");

    double fwhm = 1.66*(ff->GetParameter(1)); // 1.66 = 2.35/sqrt(2)  par1 to full-width-at-half-max of the beam
    width.push_back(fwhm);
    xx.push_back( j*aa.dz );    // z-position of the measurement
  }

  TCanvas* c4 = new TCanvas();
  int nrows = 4;
  c4->Divide((aa.Nz + nrows-1)/nrows, nrows);
  for (int i=0; i<aa.Nz; i++){
    c4->cd(i+1);
    gc[i]->Draw("AP");
  }

  TCanvas* c3 = new TCanvas();
  TGraph *gaus = new TGraph(xx.size(), &xx[0], &width[0]);
  gaus->GetHistogram()->SetYTitle("FWHM (#mum)");
  gaus->GetHistogram()->SetXTitle("z (#mum)");
  gaus->SetMarkerStyle(21);
  gaus->Draw("APL");
}
