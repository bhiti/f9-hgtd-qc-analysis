#include <iostream>
#include <numeric>
#include "TH1F.h"
#include "TGaxis.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TH2F.h"
#include "TPad.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TH3.h"


void Test(const char* text = "Hello")
{
    cout << text << endl;
    return;
}

TH1F *OpenGraph(Float_t x0,Float_t x1,Float_t y0,Float_t y1,Char_t *title,Char_t *xtit,Char_t *ytit,Float_t xs=-1,Float_t ys=-1,Int_t ieee=0)
{
  TH1F *graph=new TH1F(title,title,10,x0,x1);
  for (int i=1; i<= graph->GetNbinsX(); i++) graph->SetBinContent(i,-1e9);
  
  graph->SetMaximum(y1);
  graph->SetMinimum(y0);
  graph->SetXTitle(xtit);
  graph->SetYTitle(ytit);
  graph->SetLabelSize(0.045,"X");
  graph->SetLabelSize(0.045,"Y");
  graph->GetXaxis()->SetTitleOffset(1.3);
  graph->GetYaxis()->SetTitleOffset(1.3);
  if(xs!=-1) graph->GetXaxis()->SetTitleSize(xs);
  if(ys!=-1) graph->GetYaxis()->SetTitleSize(ys);
  //graph->GetXaxis()->SetTitleOffset(0.05);
  //graph->GetYaxis()->SetTitleSize(0.05);
  graph->SetTitleOffset(1.,"XY");
  graph->Draw();

  if(ieee==1 || ieee==3)
    {
      TGaxis *axisX = new TGaxis(x0,y1,x1,y1,x0,x1,510,"-U");
  //axisX->SetTitle("CCE");
  axisX->SetLabelSize(0.05);
  axisX->SetTitleSize(0.05);
  axisX->SetTitleOffset(1.);
  axisX->Draw();
    }

  if(ieee==2 || ieee==3)
    {
  TGaxis *axisY = new TGaxis(x1,y0,x1,y1,y0,y1,510,"+U");
  //axisX->SetTitle("CCE");
  axisY->SetLabelSize(0.05);
  axisY->SetTitleSize(0.05);
  axisY->SetTitleOffset(1.);
  axisY->Draw();
    }

  return graph;
}

int accumulateArray (int *array, int start, int end, int startValue=0)
{
  // Sum the elements of "array" from index start to index end 
  while (start <= end)
  {
    startValue += array[start];
    start++;
  }
  return startValue;
}


// Format TH2 for plotting
TH1 *DrawTH1(TH1 *histo1d, const char *hTitle = "", float tOffX = 1, float tOffY=1, float labelSize=0.05, float lineWidth=3)
{
  // histo1d ... TH1 to modify
  // hTitle ... histo title
  // tOffX ... title offset X axis
  // tOffY ... title offset Y
  // tOffZ ... title offset Y
  // labelSize .. label size
  
  histo1d->SetStats(0);
  histo1d->GetXaxis()->SetTitleSize(labelSize);
  histo1d->GetXaxis()->SetTitleOffset(tOffX);
  histo1d->GetXaxis()->SetLabelSize(labelSize);
  histo1d->GetYaxis()->SetTitleSize(labelSize);
  histo1d->GetYaxis()->SetLabelSize(labelSize);
  histo1d->GetYaxis()->SetTitleOffset(tOffY);
 
  if (strlen(hTitle) > 0) histo1d->SetTitle(hTitle);
  histo1d->SetLineWidth(lineWidth);
  
  return histo1d;
}


// Format TH2 for plotting
TH2 *FormatTH2(TH2 *histo2d, const char *hTitle = "", float tOffX = 1, float tOffY=1, float tOffZ=1, float labelSize=0.05)
{
  // histo2d ... TH2 to modify
  // hTitle ... histo title
  // tOffX ... title offset X axis
  // tOffY ... title offset Y
  // tOffZ ... title offset Y
  // labelSize .. label size
  
  histo2d->SetStats(0);
  histo2d->GetXaxis()->SetTitleSize(labelSize);
  histo2d->GetXaxis()->SetTitleOffset(tOffX);
  histo2d->GetXaxis()->SetLabelSize(labelSize);
  histo2d->GetYaxis()->SetTitleSize(labelSize);
  histo2d->GetYaxis()->SetLabelSize(labelSize);
  histo2d->GetYaxis()->SetTitleOffset(tOffY);
  histo2d->GetZaxis()->SetTitleSize(labelSize);
  histo2d->GetZaxis()->SetLabelSize(labelSize);
  histo2d->GetZaxis()->SetTitleOffset(tOffZ);
  if (strlen(hTitle) > 0) histo2d->SetTitle(hTitle);
  histo2d->SetTitleSize(labelSize);
  
  return histo2d;
}

void FormatPad2d(TVirtualPad *pad, Float_t l=0.1, Float_t r=0.15, Float_t b=0.12, Float_t t=0.1)
{
  pad->SetMargin(l,r,b,t);
  pad->SetGrid(0,0);
  return;
}

// Format TH2 for plotting
TH2 *DrawTH2(TH2 *histo2d, const char *hTitle = "", float tOffX = 1, float tOffY=1, float tOffZ=1, float labelSize=0.05)
{
  // histo2d ... TH2 to modify
  // hTitle ... histo title
  // tOffX ... title offset X axis
  // tOffY ... title offset Y
  // tOffZ ... title offset Y
  // labelSize .. label size

  FormatTH2(histo2d, hTitle, tOffX, tOffY, tOffZ, labelSize);
  
  FormatPad2d(gPad);
//   gPad->SetMargin(0.1,0.15,0.12,0.1);
//   gPad->SetGrid(0,0);
  histo2d->Draw("COLZ");
  
  return histo2d;
}

// Draw and save one slice of TH2
// void DrawSlice (TH2 **histo2d, int xyDirection = 0, double pos = 0, int Nstart = 0, int Nend = 1, const char *hTitle = "", int save=0, const char *plotsDir = "", char *fileName = "")
// {
//   // histo2d ... source TH2
//   // xyDirection ... 0=sliceX (projY), 1=sliceY (projX)
//   // pos ... position in um where to make slices
//   // Nstart ... starting voltage (index)
//   // Nend ... end voltage (index)
//   
//   // save .. if save != 0 then the plot will be saved to $plotsdir/filename$
//   
//   int color[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
//    
//   //TH1D *hProj;
//   TH1D *hProj[20];
//   double posUsed=0; 		// Get the upper value of histo's y axis
//     
// //   cout << iPix << endl;
//   if (xyDirection) posUsed = TMath::Min(pos,histo2d[Nstart]->GetYaxis()->GetXmax());
//   else posUsed = TMath::Min(pos,histo2d[Nstart]->GetXaxis()->GetXmax());
//   
//   double hMin=0, hMax=0;
//   for (int iN=Nstart; iN<Nend; iN++)
//   {
//     //histo2d[iN]->GetYaxis()->SetRangeUser(0,200);
//     //if (histo2d[iN] == NULL) break;
//     if (xyDirection) hProj[iN] = histo2d[iN]->ProjectionX(Form("%s %.0lf V x", hTitle, aa[iF].U1[iN]), histo2d[iN]->GetYaxis()->FindBin(posUsed), histo2d[iN]->GetYaxis()->FindBin(posUsed));
//     else hProj[iN] = histo2d[iN]->ProjectionY(Form("%s %.0lf V y", hTitle, aa[iF].U1[iN]), histo2d[iN]->GetXaxis()->FindBin(posUsed), histo2d[iN]->GetXaxis()->FindBin(posUsed));
//     
//     //hMin = TMath::Min(hMin, hProj[iN]->GetBinContent(hProj[iN]->GetMinimumBin()));
//     //hMax = TMath::Max(hMax, hProj[iN]->GetBinContent(hProj[iN]->GetMaximumBin()));
//     hMin = TMath::Min(hMin, histo2d[iN]->GetBinContent(histo2d[iN]->GetMinimumBin()));
//     hMax = TMath::Max(hMax, histo2d[iN]->GetBinContent(histo2d[iN]->GetMaximumBin()));
//     
//     //if (xyDirection) DrawTH1(hProj[iN], Form("CCE at y=%.0lf #mum", posUsed));
//     //else DrawTH1(hProj[iN], Form("CCE at x=%.0lf #mum", posUsed));
//     if (xyDirection) DrawTH1(hProj[iN],Form("V_{bias} = %.0lf V", aa[iF].U1[iN]));
//     else DrawTH1(hProj[iN],Form("V_{bias} = %.0lf V", aa[iF].U1[iN]));   
//   }
//    
//   gPad->SetMargin(0.13,0.15,0.15,0.1);
//   //leg = new TLegend(0.75,0.45,0.98,0.9);
//   
//   hProj[Nstart]->SetMinimum(1.1*hMin);
//   hProj[Nstart]->SetMaximum(1.1*hMax);  
//   hProj[Nstart]->GetYaxis()->SetTitle("charge (arb.)");
//   hProj[Nstart]->SetLineColor(color[Nstart]);
//   hProj[Nstart]->Draw();
//   //leg->AddEntry(hProj[Nstart],Form("V_{bias} = %.0lf V", aa.U1[Nstart]),"l");
//   //gPad->Clear();
//   for (int iN=Nstart+1; iN<Nend; iN++)
//   {
//     hProj[iN]->SetLineColor(color[iN]);
//     hProj[iN]->Draw("same");
//     //leg->AddEntry(hProj[iN],Form("V_{bias} = %.0lf V", aa.U1[iN]),"l");
//   }
//   //hProj[0]->Delete();
//   
//   //leg->Draw();
//   
//   gPad->SetGrid(1,1);
//   gPad->BuildLegend(0.75,0.9-0.08*Nvol[iPix],0.98,0.9);
//   
//   hProj[Nstart]->SetTitle(Form("CCE at %s=%.0lf #mum", xyDirection ? "y" : "x", posUsed));
//   if (save)
//   {
//     if (strlen(plotsDir) == 0) cout << "DrawSlice: Plots not saved. No directory specified." << endl;
//     else
//     {
//       gSystem->MakeDirectory(plotsDir);
//       gSystem->MakeDirectory(Form("%s/pdf",plotsDir));
//       gSystem->MakeDirectory(Form("%s/png",plotsDir));
//       gPad->Print(Form("%s/pdf/%sslice%s_%.0lfum.pdf",plotsDir, fileName, xyDirection?"Y":"X", posUsed));
//       gPad->Print(Form("%s/png/%sslice%s_%.0lfum.png",plotsDir, fileName, xyDirection?"Y":"X", posUsed));
//     }
//   }
//   //Test();
//   return; 
// }
// 
// void DrawSlicesSame (TH2F *histo2d[10][3], int iWFstart = 0, int iWFend = 1, int xyDirection = 0, double pos = 0, int Nstart = 0, int Nend = 1, const char *hTitle = "", int save=0, const char *plotsDir = "", const char *fileName = "")
// {
//   // histo2d ... source TH2
//   // xyDirection ... 0=sliceX (projY), 1=sliceY (projX)
//   // pos ... position in um where to make slices
//   // Nstart ... starting voltage (index)
//   // Nend ... end voltage (index)
//   
//   // save .. if save != 0 then the plot will be saved to $plotsdir/filename$
//   
//   
//   //histo2d[iWFstart][iWFend]->Draw("COLZ");
//   //histo2d[0]->Draw("COLZ");
//   //histo2d->Draw("COLZ");
//   
//   int color[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
//    
//   TH1D *hProj[2][20];
//   double posUsed=0; 		// Get the upper value of histo's y axis
//    
//   if (xyDirection) posUsed = TMath::Min(pos,histo2d[iWFstart][Nstart]->GetYaxis()->GetXmax());
//   else posUsed = TMath::Min(pos,histo2d[iWFstart][Nstart]->GetXaxis()->GetXmax());
//   double hMin=0, hMax=0;
//   for (int iWF = iWFstart; iWF< iWFend; iWF++)
//   {
//     for (int iN=Nstart; iN<Nend; iN++)
//     {
//       //histo2d[iN]->GetYaxis()->SetRangeUser(0,200);
//       //if (histo2d[iN] == NULL) break;
//       if (xyDirection) hProj[iWF][iN] = histo2d[iWF][iN]->ProjectionX(Form("%s %.0lf V pix%d", hTitle, aa[iPix].U1[iN], iWF), histo2d[iWF][iN]->GetYaxis()->FindBin(posUsed), histo2d[iWF][iN]->GetYaxis()->FindBin(posUsed));
//       else hProj[iWF][iN] = histo2d[iWF][iN]->ProjectionY(Form("%s %.0lf V pix%d", hTitle, aa[iPix].U1[iN], iWF), histo2d[iWF][iN]->GetXaxis()->FindBin(posUsed), histo2d[iWF][iN]->GetXaxis()->FindBin(posUsed));
// 	
//       //hMin = TMath::Min(hMin, hProj[iWF][iN]->GetBinContent(hProj[iN][iWF]->GetMinimumBin()));
//       //hMax = TMath::Max(hMax, hProj[iWF][iN]->GetBinContent(hProj[iN][iWF]->GetMaximumBin()));
//       hMin = TMath::Min(hMin, histo2d[iWF][iN]->GetBinContent(histo2d[iWF][iN]->GetMinimumBin()));
//       hMax = TMath::Max(hMax, histo2d[iWF][iN]->GetBinContent(histo2d[iWF][iN]->GetMaximumBin()));
//       
//       //if (xyDirection) DrawTH1(hProj[iWF][iN], Form("CCE at y=%.0lf #mum", posUsed));
//       //else DrawTH1(hProj[iWF][iN], Form("CCE at x=%.0lf #mum", posUsed));
//       //if (xyDirection) DrawTH1(hProj[iWF][iN],Form("pix %d, V = %.0lf V", iWF, -aa[0].U1[iN]));
//       //else DrawTH1(hProj[iWF][iN],Form("pix %d, V = %.0lf V", iWF, -aa[0].U1[iN]));   
//       DrawTH1(hProj[iWF][iN],Form("V = %.0lf V", aa[iPix].U1[iN]));   
//     }
//   }
// 	
//   gPad->SetMargin(0.13,0.2,0.15,0.1);
//   //leg = new TLegend(0.75,0.45,0.98,0.9);
//   
//   hProj[iWFstart][Nstart]->SetMinimum(1.1*hMin);
//   hProj[iWFstart][Nstart]->SetMaximum(1.1*hMax);  
//   hProj[iWFstart][Nstart]->GetYaxis()->SetTitle("charge (arb.)");
//   hProj[iWFstart][Nstart]->SetLineColor(color[Nstart]);
//   hProj[iWFstart][Nstart]->Draw();
//   //leg->AddEntry(hProj[iWF][Nstart],Form("V_{bias} = %.0lf V", aa.U1[Nstart]),"l");
//   //gPad->Clear();
//   
//   for (int iN=Nstart+1; iN<Nend; iN++)
//   {
//     hProj[iWFstart][iN]->SetLineColor(color[iN]);
//     hProj[iWFstart][iN]->Draw("same");
//     //leg->AddEntry(hProj[iN],Form("V_{bias} = %.0lf V", aa.U1[iN]),"l");
//   }
//   
//   for (int iWF = iWFstart+1; iWF< iWFend; iWF++)
//   {
//     for (int iN=Nstart; iN<Nend; iN++)
//     {
//       hProj[iWF][iN]->SetLineColor(color[iN]);
//       hProj[iWF][iN]->SetLineStyle(1+iWF);
//       hProj[iWF][iN]->Draw("same");
//     }
//   }
//   
//   gPad->SetGrid(1,1);
//   gPad->BuildLegend(0.75,0.9-0.08*Nvol[iPix]*(sqrt(iWFend-iWFstart)),0.98,0.9);
//   
//   hProj[iWFstart][Nstart]->SetTitle(Form("CCE at %s=%.0lf #mum", xyDirection ? "y" : "x", posUsed));
//   if (save)
//   {
//     if (strlen(plotsDir) == 0) cout << "DrawSlice: Plots not saved. No directory specified." << endl;
//     else
//     {
//       gSystem->MakeDirectory(plotsDir);
//       gSystem->MakeDirectory(Form("%s/pdf",plotsDir));
//       gSystem->MakeDirectory(Form("%s/png",plotsDir));
//       gPad->Print(Form("%s/pdf/%sslice%s_%.0lfum.pdf",plotsDir, fileName, xyDirection?"Y":"X", posUsed));
//       gPad->Print(Form("%s/png/%sslice%s_%.0lfum.png",plotsDir, fileName, xyDirection?"Y":"X", posUsed));
//     }
//   }
//   //Test();
//   return; //*/
// 
//   
// }

TH2F *shiftXY (TH2F *histo2d, double dx=0, double dy=0)
{
  TH2F *hCopy = (TH2F *)(histo2d->Clone());
  
  int dBinX = dx / histo2d->GetXaxis()->GetBinWidth(1);
  int dBinY = dy / histo2d->GetYaxis()->GetBinWidth(1);
  
  int nBinsX = histo2d->GetNbinsX();
  int nBinsY = histo2d->GetNbinsY();
  
  for (int iBinX=1; iBinX <= nBinsX; iBinX++)
    for (int iBinY=1; iBinY <= nBinsY; iBinY++)
    {
      if (((iBinX-dBinX) > 0 && (iBinX - dBinX) <= nBinsX) && ((iBinY - dBinY) > 0 && (iBinY - dBinY) <= nBinsY)) 
      	hCopy->SetBinContent (iBinX, iBinY, histo2d->GetBinContent(iBinX-dBinX, iBinY-dBinY));
      else hCopy->SetBinContent (iBinX, iBinY,1e-6);
    }
  
  return hCopy;
}

double GraphGetEdgeRel (TGraph *g, double p, double rising=true, bool first=false)
{
  // find x of the first rising edge above p-percent of maximum. Linear interpolation within closest data points.
  // Points in the graph ((x1,y1), (x2,y2) ...) must be sorted with x-values in ascending order (x1<x2<...) - see SortGraph below

  double ymax = TMath::MaxElement(g->GetN(), g->GetY());
  double n = p*ymax;
  if (ymax < n)
  {
//     cout << "Warning: RisingEdgeAbs - no bin with more than " << n << " entries" << endl;
    return -1111;
  }

  double x1,x2,y1,y2,k;
  double returnX = -1111;

  for (int j=2; j<=g->GetN(); j++)
  {
    // linear interpolation:
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1

    g->GetPoint(j-1, x1, y1);
    g->GetPoint(j,   x2, y2);

    if (rising == true){
      if ((y1 < n) && (y2 > n)) {
        k = (y2-y1) / (x2-x1);
        returnX = (n - y1) / k + x1;
        if (first) break;
      }
    }
    else{
      if ((y1 > n) && (y2 < n)) {
        k = (y2-y1) / (x2-x1);
        returnX = (n - y1) / k + x1;
        if (first) break;
      }
    }
  }

  return returnX;
}


double Profile_RisingEdgeAbs (TH1 *h, double n, bool first=false)
{
  // find x of a point where histogram has more than n entries in a bin. Linear interpolation within a bin.
  // offsetCompensation = 1 ... premakne graf po y, tako da ima najmanjši bin vrednost 0
  
  double ymax = h->GetBinContent(h->GetMaximumBin());			// Globalni maksimum zbranega naboja
  if (ymax < n) 
  {
//     cout << "Warning: RisingEdgeAbs - no bin with more than " << n << " entries" << endl;
    return -1111;
  }
  
  double x1,x2,y1,y2,k;		  
  double returnX = -1111;

  for (int j=2; j<=h->GetNbinsX(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
    
    y1 = h->GetBinContent(j-1);
    y2 = h->GetBinContent(j);
    x1 = h->GetBinCenter(j-1);
    x2 = h->GetBinCenter(j);
    
    if ((y1 < n) && (y2 > n)) {
      k = (y2-y1) / (x2-x1);
      returnX = (n - y1) / k + x1;
      if (first) break;
    }
  }
  
  return returnX;
}

double GraphRisingEdgeAbs (TGraph *g, double n, bool first=false)
{
  // find x of the first rising edge above n entries. Linear interpolation within closest data points.
  // Points in the graph ((x1,y1), (x2,y2) ...) must be sorted with x-values in ascending order (x1<x2<...) - see SortGraph below
  
  double ymax = TMath::MaxElement(g->GetN(), g->GetY());
  if (ymax < n) 
  {
//     cout << "Warning: RisingEdgeAbs - no bin with more than " << n << " entries" << endl;
    return -1111;
  }
  
  double x1,x2,y1,y2,k;		  
  double returnX = -1111;

  for (int j=2; j<=g->GetN(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
    
    g->GetPoint(j-1, x1, y1);
    g->GetPoint(j,   x2, y2);
    
    if ((y1 < n) && (y2 > n)) {
      k = (y2-y1) / (x2-x1);
      returnX = (n - y1) / k + x1;
      if (first) break;
    }
  }
  
  return returnX;
}

double Profile_FallingEdgeAbs (TH1 *h, double n, double x0=0, int first=0)
{
  // find x of a point where histogram has less than n entries in a bin. Linear interpolation within a bin. Linear interpolation within a bin.
  // offsetCompensation = 1 ... premakne graf po y, tako da ima najmanjši bin vrednost 0
  
  
  double ymax = h->GetMaximum();			// Globalni maksimum zbranega naboja
  double ymin = h->GetMinimum();			// Globalni maksimum zbranega naboja
  if (ymax < n) 
  {
    cout << "Warning: FallingEdgeAbs - no bin with more than n entries" << endl;
    return -1111;
  }
  
  double x1,x2,y1,y2,k;	
  
  double x=0;
  
  for (int j = 1+h->FindBin(x0); j<=h->GetNbinsX(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
      
    y1 = h->GetBinContent(j-1);
    y2 = h->GetBinContent(j);
    x1 = h->GetBinCenter(j-1);
    x2 = h->GetBinCenter(j);
    
//     cout << "." << y1 << " " << y2 << endl;
    
    if ((y1 >= n) && (y2 < n)) 
    {    
      k = (y2-y1) / (x2-x1);
      x = (n - y1) / k + x1;
      if (first) return x;
    }
  }
  
  return x;
}


double Profile_RisingEdgeRel (TH1 *h, double p, int first=0, int offsetCompensation=0)
{
  // find x of a point where histogram crosses [p] of maximum height. Linear interpolation within a bin.
  // first .. TRUE: return first rising edge, FALSE: return last rising edge
  // offsetCompensation = 1 ... premakne graf po y, tako da ima najmanjši bin vrednost 0
  
  if (p<0 || p>1) 
  {
    cout << "Error determining profile left face. Passed parameter p out of range [0,1]" << endl;
    return -1;
  }
  
  double c=0;
  if (offsetCompensation) c = -h->GetBinContent(h->GetMinimumBin());	
  
  double ymax = h->GetBinContent(h->GetMaximumBin());			// Globalni maksimum zbranega naboja
  double x1,x2,y1,y2,k;		  

  double returnX = -1111;

  for (int j=2; j<=h->GetNbinsX(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
    
    y1 = h->GetBinContent(j-1);
    y2 = h->GetBinContent(j);
    x1 = h->GetBinCenter(j-1);
    x2 = h->GetBinCenter(j);
    
    k = (y2-y1) / (x2-x1);
        
    if ((y1+c < p*(ymax+c)) && (y2+c > p*(ymax+c))) {
      returnX = (p*ymax - y1) / k + x1;
      if (first) break;
//       return( p*ymax - y1) / k + x1;
    }
  }
  
//   return -1111;
  return returnX;
}

double Profile_RisingEdgeRel (TGraph *g, double p, int offsetCompensation=0)
{
  // find x of a point where histogram first crosses [p] of maximum height. Linear interpolation within a bin.
  // offsetCompensation = 1 ... premakne graf po y, tako da ima najmanjši bin vrednost 0
  
  if (p<0 || p>1) 
  {
    cout << "Error determining profile left face. Passed parameter p out of range [0,1]" << endl;
    return -1;
  }
  
  double c=0;
  if (offsetCompensation) c = -TMath::MinElement(g->GetN(),g->GetY());
  
  double ymax = TMath::MaxElement(g->GetN(),g->GetY());			// Globalni maksimum zbranega naboja
  double x1,x2,y1,y2,k;		  
   
  for (int j=2; j<=g->GetN(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
    
    y1 = g->GetY()[j-1];
    y2 = g->GetY()[j];
    x1 = g->GetX()[j-1];
    x2 = g->GetX()[j];
    
    k = (y2-y1) / (x2-x1);
        
    if ((y1+c < p*(ymax+c)) && (y2+c > p*(ymax+c))) 
      return( p*ymax - y1) / k + x1;
  }
  
  return -1111;
}

double Profile_FallingEdgeRel (TH1 *h, double p, double x0=0, int first=0, int offsetCompensation=0)
{
  // find x of a point where histogram last crosses [p] of maximum height. Linear interpolation within a bin.
  // first = 1 ... vrne prvi najdeni bin
  // offsetCompensation = 1 ... premakne graf po y, tako da ima najmanjši bin vrednost 0
  
  if (p<0 || p>1) 
  {
    cout << "Error determining profile left face. Passed parameter p out of range [0,1]" << endl;
    return -1111;
  }
  
  double c=0;
  if (offsetCompensation) c = -h->GetBinContent(h->GetMinimumBin());	
  
  double ymax = h->GetBinContent(h->GetMaximumBin());			// Globalni maksimum zbranega naboja
  double x1,x2,y1,y2,k;	
  
  double x=0;
  
  for (int j = 1+h->FindBin(x0); j<=h->GetNbinsX(); j++)
  {
    // Poišči začetek. Točen položaj izračunamo z linearno interpolacijo med sosednjima binoma
    // k = (y2-y1)/(x2-x1)
    // x = (y-y1)/k + x1
      
    y1 = h->GetBinContent(j-1);
    y2 = h->GetBinContent(j);
    x1 = h->GetBinCenter(j-1);
    x2 = h->GetBinCenter(j);
    
    k = (y2-y1) / (x2-x1);
    
    if ((y1+c > p*(ymax+c)) && (y2+c < p*(ymax+c))) 
    {
      x = (p*ymax - y1) / k + x1;
      if (first) return x;
    }
  }
  
  return x;
}

double Profile_Width (TH1 *h, double p, int first=0, int offsetCompensation=0)
{
  // return full width of a histogram at [p] of maximum height, for example p=0.5 -> return FWHM
  // if first=TRUE measure width at the first falling edge, otherwise at the last falling edge in the histogram
  
  if (p<0 || p>1) 
  {
    cout << "Error determining profile width. Passed parameter p out of range [0,1]" << endl;
    return -1;
  }
  
  double xr = Profile_RisingEdgeRel(h,p,offsetCompensation);
  double width = Profile_FallingEdgeRel(h,p,xr,first,offsetCompensation) - xr;
  
  return width;
}

double Profile_Width_Thr (TH1 *h, double p, double globalMax, double thr, int first=0, int offsetCompensation=0)
{
  // return full width of a histogram at [p] of maximum height, for example p=0.5 -> return FWHM
  // ignore the measurement if max peak height is too low (less than globalmax*thr (then width = 0))
  // if first=TRUE measure width at the first falling edge, otherwise at the last falling edge in the histogram
    
  double sliceMax = h->GetBinContent(h->GetMaximumBin());
  
  if (sliceMax*p > globalMax*thr) return Profile_Width (h, p, 1, offsetCompensation);
  else if (sliceMax < globalMax*thr) return 0.;
  else return Profile_FallingEdgeAbs(h,globalMax*thr,1,1) - Profile_RisingEdgeAbs(h,globalMax*thr);
  
  return -1.;
}
	

	
// double GetNeff(int ip, double p)
// {
//   // izračuna Neff za meritev ip
//   // metoda za določitev širine je FW at p% max
// 
//   double Wdepl[100] = {0};	// Sirina obmocja z >50% maksimalnega CCE
//   
//   for (int i=0; i<Nvol[ip]; i++) 
//   {
//     TH1D* hslice = c2d[ip][i]->ProjectionY(Form("Slice Pix %d V bias = %.0f V", ip, aa[ip].U1[i]),1,1);
// 	double thr = 0.06;	// procenti pod katerimi se signal ignorira
//     Wdepl[i] = Profile_Width_Thr (hslice, p, globalMax[ip], thr, 1);	// bolj napredna funkcija
//   }
//     
//   TGraph *g = new TGraph(Nvol[ip], biasVoltage[ip], Wdepl);
//   TF1 *func = new TF1("func","[0]*TMath::Sqrt(x)+[1]",0,120);
//   g->Fit("func","RQ");
//  
//   // Izračunaj Neff = 2*epsilon*epsilon0/e0/[0]^2 - enote *1e12*1e-6 cm-3
//   double Neff_d = 2*11.7*8.85e-12/1.6e-19/ (func->GetParameter(0))**2 * 1e6;
//   //cout << ip << ": " << Neff_d << endl;
//   //g->Draw("APL");
//   g->Delete();
//   func->Delete();
//   
//   return Neff_d;
// }
// 
// void GetNeffAvg(int ip, double p, double dp, double *mean, double *rms)
// {
//   double Neff_d[3];
//   double A=1e-14;
//   Neff_d[0] = GetNeff(ip, p)*A;
//   Neff_d[1] = GetNeff(ip, p+dp)*A;
//   Neff_d[2] = GetNeff(ip, p-dp)*A;
//   
//   /*
//   *mean = TMath::Mean(3, Neff_d);
//   *rms = TMath::RMS(3, Neff_d);
//   */
//   *mean = TMath::Mean(1, Neff_d);
//   *rms = TMath::RMS(3, Neff_d);
//   
//   return;
// }

TGraphErrors Graph_NvsPhi2 (int n, double *phi, double *Neff, double *ex, double *ey)
{
  // phi .. fluence
  TGraphErrors g(n, phi, Neff,ex,ey);
  
  double labelSize=0.06;
  g.SetTitle("N_{eff} vs. fluence");
  g.GetXaxis()->SetTitle("#Phi (10^{14} neq cm^{-2})");
  g.GetXaxis()->SetTitleSize(labelSize);
  g.GetXaxis()->SetLabelSize(labelSize);
  g.GetXaxis()->SetTitleOffset(0.1);
  g.GetYaxis()->SetTitleOffset(0.1);
  g.GetYaxis()->SetTitle("N_{eff} (10^{14} cm^{-3})");
  g.GetYaxis()->SetTitleSize(labelSize);
  g.GetYaxis()->SetLabelSize(labelSize);
  g.SetMarkerStyle(20);
  return g;  
}

TGraphErrors *Graph_NvsPhi (int n, double *phi, double *Neff, double *ex, double *ey)
{
  // phi .. fluence
  TGraphErrors *g = new TGraphErrors(n, phi, Neff,ex,ey);
    
  double labelSize=0.06;
  g->SetTitle("N_{eff} vs. fluence");
  g->GetXaxis()->SetTitle("#Phi (10^{14} neq cm^{-2})");
  g->GetXaxis()->SetTitleSize(labelSize);
  g->GetXaxis()->SetLabelSize(labelSize);
  g->GetXaxis()->SetTitleOffset(1);
  g->GetYaxis()->SetTitleOffset(1);
  g->GetYaxis()->SetTitle("N_{eff} (10^{14} cm^{-3})");
  g->GetYaxis()->SetTitleSize(labelSize);
  g->GetYaxis()->SetLabelSize(labelSize);
  g->SetMarkerStyle(20);
  return g;  
}

void NvsPhi_Fit(TGraphErrors *g)
{
  //cout << g->GetN() << endl;
  TF1 *f = new TF1("f","[0]*(1 - [1]*(1-exp(-[2]*x))) + [3]*x",0,20);
  
  double p0 = g->GetY()[0];
  double p1 = 1;
  //double p2 = 1.0e-14 * 1e14;
  double p2 = 1/g->GetX()[TMath::LocMin(g->GetN(),g->GetY())];;	// take fluence with lowest neff as decay constant
  double p3 = 0.02;
  cout << p0 << " " << p1 << " " << p2 << " " << p3 << endl;
  f->SetParameters (p0,p1,p2,p3);
  //f->FixParameter(0,p0);
  f->SetParLimits(1, 0, 2);
  f->SetParLimits(2, 0, 1e3);
  f->SetParLimits(3, 0, 1);
  f->FixParameter(3, 0.02);

  g->Fit("f");
  cout << f->GetParameter(3) << endl;
  
  
  return;
}

void DrawTH2withBinContent(TH2* h, Float_t texX = 0.23, Float_t texY= 0.18, const char* opt="colz")
{
  h->Draw(opt);
  int Nx=h->GetNbinsX(), Ny=h->GetNbinsY();
  for (int ix=1; ix<=Nx; ix++){
    for (int iy=1; iy<=Ny; iy++){
      double z=h->GetBinContent(ix, iy);
      TString s = Form("%.0lf", z);
      
      double xc = ((TAxis*)h->GetXaxis())->GetBinCenter(ix);
      double yc = ((TAxis*)h->GetYaxis())->GetBinCenter(iy);
      TLatex tex;
      tex.SetTextFont(42); tex.SetTextSize(0.05);  tex.SetTextColor(1);
      tex.DrawLatex(xc-texX, yc-texY, s.Data());
        
    }
  }
  return;  
}

TH2F* GetTH2(TH3F* h3d, int binStart, int binEnd=-1, const char* plane="zx")
{
  if (binEnd == -1) binEnd=binStart;

  TH2F* hFinal;

  if      (strncmp(plane,"zx",2) == 0) h3d->GetYaxis()->SetRange(binStart, binEnd);
  else if (strncmp(plane,"xz",2) == 0) h3d->GetYaxis()->SetRange(binStart, binEnd);
  else if (strncmp(plane,"xy",2) == 0) h3d->GetZaxis()->SetRange(binStart, binEnd);
  else if (strncmp(plane,"yx",2) == 0) h3d->GetZaxis()->SetRange(binStart, binEnd);
  else if (strncmp(plane,"zy",2) == 0) h3d->GetXaxis()->SetRange(binStart, binEnd);
  else if (strncmp(plane,"yz",2) == 0) h3d->GetXaxis()->SetRange(binStart, binEnd);
  else {
    cout << "GetTH2: select valid projection plane (e.g. xy). You selected " << plane << endl;
    return hFinal;
  }
  
  TString hname(Form("%s_p%s_%d_%d", h3d->GetName(), plane, binStart, binEnd));
  TH2F* h2d = (TH2F*)(h3d->Project3D(plane));

  bool flipXY = false;
  if      (strncmp(plane,"zx",2) == 0) flipXY=true;
  else if (strncmp(plane,"yx",2) == 0) flipXY=true;
  else if (strncmp(plane,"zy",2) == 0) flipXY=true;

  if (! flipXY) hFinal = (TH2F*)(h2d->Clone(hname.Data()));
  else {
    hFinal = new TH2F(hname.Data(), "", h2d->GetNbinsY(), h2d->GetYaxis()->GetXmin(), h2d->GetYaxis()->GetXmax(), h2d->GetNbinsX(), h2d->GetXaxis()->GetXmin(), h2d->GetXaxis()->GetXmax());
    hFinal->GetXaxis()->SetTitle(h2d->GetYaxis()->GetTitle());
    hFinal->GetYaxis()->SetTitle(h2d->GetXaxis()->GetTitle());
    for (int ix=0; ix<=h2d->GetNbinsX(); ix++){
      for (int iy=0; iy<=h2d->GetNbinsY(); iy++){
        hFinal->SetBinContent(iy, ix, h2d->GetBinContent(ix, iy));
      }
    }
  }
  hFinal->SetTitle("");
  hFinal->GetZaxis()->SetTitle("charge (ke^{-})");

//   TString hname(Form("%s_p%s_%d_%d", h3d->GetName(), plane, binStart, binEnd));
//   TH2F* h2d = (TH2F*)(h3d->Project3D(plane))->Clone(hname.Data());
//   h2d->SetTitle("");
//   h2d->GetZaxis()->SetTitle("charge (ke^{-})");

//   return h2d;
  return hFinal;
}

TH1F* GetTH1(TH3* h3d, const char* plane, int binStartX, int binEndX=-1, int binStartY=0, int binEndY=-1)
{
  if (binEndX == -1) binEndX=binStartX;
  if (binEndY == -1) binEndY=binStartY;
  
  TString hname(Form("%s_p%s_x%d-%d_y%d-%d", h3d->GetName(), plane, binStartX, binEndX, binStartY, binEndY));

  if      (strncmp(plane,"x",1) == 0) {
    h3d->GetYaxis()->SetRange(binStartX, binEndX);
    h3d->GetZaxis()->SetRange(binStartY, binEndY);
  }
  else if (strncmp(plane,"y",1) == 0) {
    h3d->GetXaxis()->SetRange(binStartX, binEndX);
    h3d->GetZaxis()->SetRange(binStartY, binEndY);
  }
  else if (strncmp(plane,"z",1) == 0) {
    h3d->GetXaxis()->SetRange(binStartX, binEndX);
    h3d->GetYaxis()->SetRange(binStartY, binEndY);
  }
  else    cout << "GetTH1: Wrong plane specified. Select \"x\" or \"y\" or \"z\". You selected: " << plane << endl;
  
  TH1F* h1d = (TH1F*)(h3d->Project3D(plane))->Clone(hname.Data());

  h3d->GetXaxis()->SetRange();
  h3d->GetYaxis()->SetRange();
  h3d->GetZaxis()->SetRange();
//   h1d->SetTitle("");
//   h1d->GetYaxis()->SetTitle("charge (ke^{-})");
  h1d->SetStats(0);
//   float asd=h1d->GetMaximum(); if (asd>1.5) cout << asd << endl;
  return h1d;
}

TH1D* GetTH1(TH2* h2d, const char* plane, int binStart, int binEnd=-1)
{
  if (binEnd == -1) binEnd=binStart;
  TString hname(Form("%s_p%s_%d_%d", h2d->GetName(), plane, binStart, binEnd));

  TH1D* h1d;

  if      (strncmp(plane,"x",1) == 0)h1d = h2d->ProjectionX(hname.Data(), binStart, binEnd);
  else if (strncmp(plane,"y",1) == 0) h1d = h2d->ProjectionY(hname.Data(), binStart, binEnd);
  else    cout << "GetTH1: Wrong plane specified. Select x or y. You selected: " << plane << endl;
  
//   h1d->GetYaxis()->SetTitle("charge (ke^{-})");
  h1d->SetStats(0);
  return h1d;
}

/**
 * @brief Sort points in a TGraph so that x-values are in ascending/descending order
 * @param g graph to sort
 * @param ascending true .. ascending order; false .. descending order
 * @return sorted TGraph
 */
TGraph* SortGraph(TGraph *g, bool ascending=true)
{
  vector<double> x, y;
  for (int i=0; i<g->GetN(); i++){
    double a,b;
    g->GetPoint(i, a, b);
    x.push_back(a);
    y.push_back(b);
  }

  vector<size_t> indices(x.size());
  iota(indices.begin(), indices.end(), 0); // Initialize indices with 0, 1, 2, ...

  if (ascending){
    std::sort(indices.begin(), indices.end(), [&x](size_t a, size_t b) {
      return x[a] < x[b];
    });
  }
  else{
    std::sort(indices.begin(), indices.end(), [&x](size_t a, size_t b) {
      return x[a] > x[b];
    });
  }

  for (int i=0; i<g->GetN(); i++) g->SetPoint(i, x[indices[i]], y[indices[i]]);

  return g;
}
