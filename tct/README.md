# HGTD Quality Control - TCT

Analysis of TCT measurements for Irradiation Tests in HGTD production

## Requirements

- CERN ROOT 6
- TCTanalyse library: https://particulars.si/TCTAnalyse/TCTAnalyse.V2.2.zip
Tested on Ubuntu 22.04

## Setup

Run `setup.sh` before first use

## Usage

`root -l simple2d.cpp`
`.x fitLGAD.cpp`
`errorLGAD.cpp`
